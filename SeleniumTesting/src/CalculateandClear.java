

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class CalculateandClear {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  

  @Before
  public void setUp() throws Exception {
	  System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
	//System.setProperty("webdriver.gecko.driver", "<Path to your WebDriver>");
    //driver = new FirefoxDriver();
	  driver = new ChromeDriver();
    baseUrl = "http://localhost:8082/OhmsLaw/Ohms.jsp";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCalculateandClear() throws Exception {
    driver.get("http://localhost:8082/OhmsLaw/WebContent/Ohms.jsp");
    driver.findElement(By.id("voltage")).click();
    driver.findElement(By.id("voltage")).clear();
    driver.findElement(By.id("voltage")).sendKeys("23");
    driver.findElement(By.id("resistance")).click();
    driver.findElement(By.id("resistance")).clear();
    driver.findElement(By.id("resistance")).sendKeys("34.2");
    driver.findElement(By.id("calculate")).click();
    //driver.findElement(By.id("clear")).click();
    double voltageInput = Double.parseDouble(driver.findElement(By.id("voltage")).getAttribute("value"));
    double resistanceInput = Double.parseDouble(driver.findElement(By.id("resistance")).getAttribute("value"));

    double currentResultWeb = Double.parseDouble(driver.findElement(By.id("current")).getAttribute("value"));
    double powerResultWeb = Double.parseDouble(driver.findElement(By.id("power")).getAttribute("value"));
    

    double currentResult = voltageInput/resistanceInput;
    double powerResult = Math.pow(voltageInput, 2)/resistanceInput;
    
    
    Assert.assertEquals(currentResult, currentResultWeb);
    Assert.assertEquals(powerResult, powerResultWeb);
    driver.quit();
    
   // assertEquals(currentResult, currentResultWeb);
    //assertEquals(powerResult, powerResultWeb);
    
    }
}
